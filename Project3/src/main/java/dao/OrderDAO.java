package dao;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -implementeaza interogarile efectuate asupra tabelului order;
 * 
 * Atribute:
 *-un atribut private static final de tip logger: LOGGER;
 *-3 atribute private static final de tip String:
 *       -insertStatementString(interogarea folosita pentru a introduce o comanda noua in baza de date);
 *       -findStatementString(interogarea folosita pentru a gasi un client in baza de date);
 *       -clearStatementString(interogarea folosita pentru a sterge toate comenziile din baza de date);
 * 
 */

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.Client;
import model.Order;
import model.Product;

public class OrderDAO {

	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO warehouse.order (id_client,id_product,quantity,order_price)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM warehouse.order where id = ?";
	private final static String clearStatementString = "TRUNCATE warehouse.order";
	
	/**
	 * metoda cauta si returneaza comanda cu id-ul egal cu parametrul orderId;
	 * @param orderId
	 * @return foundOrder
	 */
	
	public static Order findById(int orderId) {
	    Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderId);
			rs = findStatement.executeQuery();
			rs.next();

			int id_client=rs.getInt("id_client");
			int id_product=rs.getInt("id_product");
			int quantity=rs.getInt("quantity");
			toReturn = new Order(orderId, id_client, id_product, quantity);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * -metoda insereaza comada transmisa ca parametru in baza de date si returneaza pozitia pe care a fost inserata(daca comanda nu poate fi inserat returneaza -1);
     * -metoda genereaza si un fisier text pentru factura;
     * -daca comanda s-a efectuat cu succes afiseaza un mesaj,altfel daca stocul produsului e mai mic deact cantitatea afiseaza un mesaj de eroare;
	 * @param order
	 * @return insertedId
	 */
	
	public static int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			Product p=ProductDAO.findById(order.getId_product());
			Client c=ClientDAO.findById(order.getId_client());
			if(order.getQuantity()<=p.getStock())
			{
				insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
				insertStatement.setInt(1, order.getId_client());
				insertStatement.setInt(2, order.getId_product());
				insertStatement.setInt(3, order.getQuantity());
				insertStatement.setFloat(4, order.getOrder_price());
				insertStatement.executeUpdate();
				
				ResultSet rs = insertStatement.getGeneratedKeys();
				if (rs.next()) {
					insertedId = rs.getInt(1);
				}

				ProductDAO.editById(order.getId_product(),"", 0, p.getStock()-order.getQuantity());
			
				String fileName="bill"+insertedId+".txt";
				try{
				    PrintWriter writer = new PrintWriter(fileName, "UTF-8");
				    writer.println("bill"+insertedId);
				    writer.println("");
				    writer.println(c.getName());
				    writer.println(c.getAddress());
				    writer.println(c.getEmail());
				    writer.println("");
				    writer.println("Product:"+p.getName()+" Price:"+p.getPrice()+" Quantity:"+order.getQuantity()+" Total price:"+order.getOrder_price());
				    writer.close();
				} 
				catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
				JOptionPane.showMessageDialog(null,"Order successful");
			}
			else
			{
				JOptionPane.showMessageDialog(null,p.getName()+ " is under stock.Stock:"+p.getStock());
			}
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	/**
	 * -metoda sterge toate comenziile din baza de date;
	 * @return clearStatement.executeUpdate()
	 */
	
	public static int clear() {
		Connection dbConnection = ConnectionFactory.getConnection();

		int k=0;
		PreparedStatement clearStatement = null;
		try {
			clearStatement = dbConnection.prepareStatement(clearStatementString, Statement.RETURN_GENERATED_KEYS);
			k=clearStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:clear " + e.getMessage());
		} finally {
			ConnectionFactory.close(clearStatement);
			ConnectionFactory.close(dbConnection);
		}
		return k;
	}
}