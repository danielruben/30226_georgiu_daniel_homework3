package dao;


/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -implementeaza interogarile efectuate asupra tabelului client;
 * 
 * Atribute:
 * -un atribut private static final de tip logger: LOGGER;
 * -5 atribute private static final de tip String:
 *       -insertStatementString(interogarea folosita pentru a introduce un client nou in baza de date);
 *       -findStatementString(interogarea folosita pentru a gasi un client in baza de date);
 *       -updateStatementString(interogarea folosita pentru a edita un client existent in baza de date);
 *       -deleteStatementString(interogarea folosita pentru a sterge un client existent in baza de date);
 *       -clearStatementString(interogarea folosita pentru a sterge toti clienti din baza de date);
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (name,address,email)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM client where id = ?";
	private final static String updateStatementString = "UPDATE client SET name=?,address=?,email=? WHERE id= ?";
	private final static String deleteStatementString = "DELETE FROM client WHERE id= ?";
	private final static String clearStatementString = "TRUNCATE client ";

	/**
	 * -metoda cauta si returneaza clientul cu id-ul egal cu parametrul clientId
	 * @param clientId
	 * @return foundclient
	 */
	
	public static Client findById(int clientId) {
	    Client toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, clientId);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			toReturn = new Client(clientId, name, address, email);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * -metoda insereaza clientul transmis ca parametru in baza de date si returneaza pozitia pe care a fost inserat(daca clientul nu poate fi inserat returneaza -1)
	 * @param client
	 * @return insertedId
	 */
	
	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getName());
			insertStatement.setString(2, client.getAddress());
			insertStatement.setString(3, client.getEmail());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	/**
	 * -metoda editeaza un client existent in baza de date cu noiile informatii primite ca parametri;
	 * @param clientId
	 * @param name
	 * @param address
	 * @param email
	 * @return updateStatement.executeUpdate()
	 */
	
	public static int editById(int clientId,String name,String address,String email) {

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		int k=0;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			Client client=ClientDAO.findById(clientId);
			updateStatement.setInt(4, client.getId());
			if(name.length()!=0)
			{
				updateStatement.setString(1, name);
			}
			else
			{
				updateStatement.setString(1, client.getName());
			}
			if(address.length()!=0)
			{
				updateStatement.setString(2, address);
			}
			else
			{
				updateStatement.setString(2, client.getAddress());
			}
			if(email.length()!=0)
			{
				updateStatement.setString(3, email);
			}
			else
			{
				updateStatement.setString(3, client.getEmail());
			}
			k=updateStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:editById " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return k;
	}
	
	/**
	 * -metoda sterge clientul care are id-ul egal cu parametrul clientId
	 * @param clientId
	 * @return deleteStatement.executeUpdate()
	 */
	
	public static int delete(int clientId) {
		Connection dbConnection = ConnectionFactory.getConnection();

		int k=0;
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, clientId);
			k=deleteStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return k;
	}
	
	/**
	 * -metoda sterge toti clienti din baza de date;
	 * @return clearStatement.executeUpdate()
	 */
	
	public static int clear() {
		Connection dbConnection = ConnectionFactory.getConnection();

		int k=0;
		PreparedStatement clearStatement = null;
		try {
			clearStatement = dbConnection.prepareStatement(clearStatementString, Statement.RETURN_GENERATED_KEYS);
			k=clearStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:clear " + e.getMessage());
		} finally {
			ConnectionFactory.close(clearStatement);
			ConnectionFactory.close(dbConnection);
		}
		return k;
	}
}