package dao;


/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -implementeaza interogarile efectuate asupra tabelului product;
 * 
 * Atribute:
 *-un atribut private static final de tip logger: LOGGER;
 *-5 atribute private static final de tip String:
 *       -insertStatementString(interogarea folosita pentru a introduce un produs nou in baza de date);
 *       -findStatementString(interogarea folosita pentru a gasi un produs in baza de date);
 *       -updateStatementString(interogarea folosita pentru a edita un produs existent in baza de date);
 *       -deleteStatementString(interogarea folosita pentru a sterge un produs existent in baza de date);
 *       -clearStatementString(interogarea folosita pentru a sterge toate produsele din baza de date);
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Product;

public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (name,price,stock)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM product where id = ?";
	private final static String updateStatementString = "UPDATE product SET name=?,price=?,stock=? WHERE id= ?";
	private final static String deleteStatementString = "DELETE FROM product WHERE id= ?";
	private final static String clearStatementString = "TRUNCATE product";

	/**
	 *-metoda cauta si returneaza produsul cu id-ul egal cu parametrul productId 
	 * @param productId
	 * @return foundProduct
	 */
	public static Product findById(int productId) {
	    Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, productId);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			float price = rs.getFloat("price");
			int stock=rs.getInt("stock");
			toReturn = new Product(productId, name, price,stock);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * -metoda insereaza produsul transmis ca parametru in baza de date si returneaza pozitia pe care a fost inserat(daca produsul nu poate fi inserat returneaza -1)
	 * @param product
	 * @return insertedId
	 */
	
	public static int insert(Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getName());
			insertStatement.setFloat(2, product.getPrice());
			insertStatement.setInt(3, product.getStock());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	/**
	 * -metoda editeaza un produs existent in baza de date cu noiile informatii primite ca parametri;
	 * @param productId
	 * @param name
	 * @param price
	 * @param stock
	 * @return updateStatement.executeUpdate()
	 */
	
	public static int editById(int productId,String name,float price,int stock) {

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		int k=0;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			Product product=ProductDAO.findById(productId);
			updateStatement.setInt(4, product.getId());
			if(name.length()!=0)
			{
				updateStatement.setString(1, name);
			}
			else
			{
				updateStatement.setString(1, product.getName());
			}
			if(price>0)
			{
				updateStatement.setFloat(2, price);
			}
			else
			{
				updateStatement.setFloat(2, product.getPrice());
			}
			if(stock>=0)
			{
				updateStatement.setFloat(3, stock);
			}
			else
			{
				updateStatement.setFloat(3, product.getStock());
			}
			k=updateStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return k;
	}
	
	/**
	 * -metoda sterge clientul care are id-ul egal cu parametrul productId
	 * @param productId
	 * @return deleteStatement.executeUpdate()
	 */
	
	public static int delete(int productId) {
		Connection dbConnection = ConnectionFactory.getConnection();

		int k=0;
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, productId);
			k=deleteStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return k;
	}
	
	/**
	 * -metoda sterge toate produsele din baza de date;
	 * @return clearStatement.executeUpdate()
	 */
	
	public static int clear() {
		Connection dbConnection = ConnectionFactory.getConnection();

		int k=0;
		PreparedStatement clearStatement = null;
		try {
			clearStatement = dbConnection.prepareStatement(clearStatementString, Statement.RETURN_GENERATED_KEYS);
			k=clearStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:clear " + e.getMessage());
		} finally {
			ConnectionFactory.close(clearStatement);
			ConnectionFactory.close(dbConnection);
		}
		return k;
	}
}
