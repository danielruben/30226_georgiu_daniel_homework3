package model;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -folosita pentru extragerea informatiilor din tabelul product.
 * 
 * Are aceeasi atribute ca tabelul order din baza de date:
 * -4 atribut de tip int:
 *       -id,care e unic si prin care comanda se identifica;
 *       -id_client;
 *       -id_product;
 *       -quantity;
 * -un atribut de tip float: order_price;
 * -atributele sunt private,accesibile prin metodele get si set;
 */

public class Product
{
	private int id;
	private String name;
	private float price;
	private int stock;
	
	public Product(int id, String name, float price,int stock) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.stock=stock;
	}
	
	public Product(String name, float price,int stock) {
		this.name = name;
		this.price = price;
		this.stock=stock;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * -metoda toString a clasei Object este suprascrisa;
     * -metoda afiseaza id-ul,id-ul clientului,id-ul produsului,cantitatea  produsului si pretul comenzii
	 */
	
	public String toString()
	{
		return "Product [id=" + id + ", name=" + name + ", price=" + price + ", stock=" + stock + "]";
	}
}
