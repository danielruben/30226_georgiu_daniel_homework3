package model;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * 
 * -folosita pentru extragerea informatiilor din tabelul order.
 * 
 * Are aceeasi atribute ca tabelul product din baza de date:
 * -un atribut de tip int:id,care e unic si prin care produsul se identifica;
 * -un atrbut de tip String:name;
 * -un atribut de tip float:price;
 * -un atribut de tip int:stock;
 * -atributele sunt private,accesibile prin metodele get si set; 
 * 
 */

import dao.ProductDAO;

public class Order
{
	private int id;
	private int id_client;
	private int id_product;
	private int quantity;
	private float order_price;
	
	public Order(int id, int id_client, int id_product, int quantity) {
		this.id = id;
		this.id_client = id_client;
		this.id_product = id_product;
		this.quantity = quantity;
		this.order_price=quantity*ProductDAO.findById(id_product).getPrice();
	}
	
	public Order(int id_client, int id_product, int quantity) {
		this.id_client = id_client;
		this.id_product = id_product;
		this.quantity = quantity;
		this.order_price=quantity*ProductDAO.findById(id_product).getPrice();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	public int getId_product() {
		return id_product;
	}

	public void setId_product(int id_product) {
		this.id_product = id_product;
	}



	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getOrder_price() {
		return order_price;
	}

	public void setOrder_price(float order_price) {
		this.order_price = order_price;
	}

	/**
	 * -metoda toString a clasei Object este suprascrisa;
     * -metoda afiseaza id-ul,numele,pretul si stocul produsului
	 * 
	 */
	
	public String toString()
	{
		return "Order [id=" + id + ", id_client=" + id_client + ", id_product=" + id_product + 
				", quantity=" + quantity + ", order_price=" + order_price +"]";
	}
}