package model;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -folosita pentru extragerea informatiilor din tabelul client.
 * 
 * Are aceeasi atribute ca tabelul client din baza de date:
 *  -un atribut de tip int:id,care e unic si prin care clientul se identifica;
 *  -3 atribute de tip String:name,address si email;
 *  -atributele sunt private,accesibile prin metodele get si set;
 *
 */

public class Client
{
	private int id;
	private String name;
	private String address;
	private String email;
	
	public Client(int id, String name, String address, String email) 
	{
		this.id = id;
		this.name = name;
		this.address = address;
		this.email = email;
	}

	public Client(String name, String address, String email) 
	{
		this.name = name;
		this.address = address;
		this.email = email;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * -metoda toString a clasei Object este suprascrisa;
     * -metoda afiseaza id-ul,numele,adresa si emailul clientului
     *
	 */
	
	public String toString()
	{
		return "Client [id=" + id + ", name=" + name + ", address=" + address + ", email=" + email + "]";
	}
}
