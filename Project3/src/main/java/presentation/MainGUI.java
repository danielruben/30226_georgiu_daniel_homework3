package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 05, 2017
 * 
 * -are 3 butoane: Client Operations, Product Operations si Order Operations;
 * -fiecare buton deschide un nou Jframe
 */

public class MainGUI {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 302, 378);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblWaredouse = new JLabel("Warehouse Management");
		lblWaredouse.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblWaredouse.setHorizontalAlignment(SwingConstants.CENTER);
		lblWaredouse.setBounds(0, 11, 274, 37);
		frame.getContentPane().add(lblWaredouse);
		
		/**
		 * -butonul deschide Jframe-ul ClientGUI;
		 */
		JButton btnNewButton = new JButton("Client Operations");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				try
				{
					ClientGUI c=new ClientGUI();
					c.setVisible(true);
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,e.getMessage());
				}
			}
		});
		btnNewButton.setBounds(47, 83, 181, 59);
		frame.getContentPane().add(btnNewButton);
		
		/**
		 * -butonul deschide Jframe-ul ProductGUI;
		 */
		JButton btnProductOperations = new JButton("Product Operations");
		btnProductOperations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					ProductGUI p=new ProductGUI();
					p.setVisible(true);
				}
				catch(Exception e1)
				{
					JOptionPane.showMessageDialog(null,e1.getMessage());
				}
			}
		});
		btnProductOperations.setBounds(47, 169, 181, 59);
		frame.getContentPane().add(btnProductOperations);
		
		/**
		 * -butonul deschide Jframe-ul OrderGUI;
		 */
		JButton btnClientOperations = new JButton("Order Operations");
		btnClientOperations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					OrderGUI o=new OrderGUI();
					o.setVisible(true);
				}
				catch(Exception e2)
				{
					JOptionPane.showMessageDialog(null,e2.getMessage());
				}
			}
		});
		btnClientOperations.setBounds(47, 255, 181, 59);
		frame.getContentPane().add(btnClientOperations);
	}
}
