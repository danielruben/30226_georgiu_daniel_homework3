package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bll.OrderBLL;
import connection.ConnectionFactory;
import model.Order;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 05, 2017
 * 
 * -are 1 buton de inserare a comenzii.
 */

public class OrderGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField_quantity;
	private JComboBox<Integer> comboBoxIdClient;
	private JComboBox<Integer> comboBoxIdProduct;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderGUI frame = new OrderGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void fill_idclient()
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			String query="SELECT id FROM client";
			statement=dbConnection.prepareStatement(query);
			rs = statement.executeQuery();
			
			while(rs.next())
			{
				comboBoxIdClient.addItem(rs.getInt("id"));
			}
		}
		catch(Exception e2)
		{
			JOptionPane.showMessageDialog(null,e2.getMessage());
		}
		finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	public void fill_idproduct()
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			String query="SELECT id FROM product";
			statement=dbConnection.prepareStatement(query);
			rs = statement.executeQuery();
			
			while(rs.next())
			{
				comboBoxIdProduct.addItem(rs.getInt("id"));
			}
		}
		catch(Exception e3)
		{
			JOptionPane.showMessageDialog(null,e3.getMessage());
		}
		finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(dbConnection);
		}
	}
	
	/**
	 * Create the frame.
	 */
	public OrderGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 150, 238);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIdclient = new JLabel("id_client:");
		lblIdclient.setBounds(45, 11, 46, 14);
		contentPane.add(lblIdclient);
		
		JLabel lblIdproduct = new JLabel("id_product:");
		lblIdproduct.setHorizontalAlignment(SwingConstants.CENTER);
		lblIdproduct.setBounds(10, 61, 110, 14);
		contentPane.add(lblIdproduct);
		
		JLabel lblQuantity = new JLabel("quantity:");
		lblQuantity.setHorizontalAlignment(SwingConstants.CENTER);
		lblQuantity.setBounds(10, 111, 110, 14);
		contentPane.add(lblQuantity);
		
		textField_quantity = new JTextField();
		textField_quantity.setColumns(10);
		textField_quantity.setBounds(22, 130, 86, 20);
		contentPane.add(textField_quantity);
		
		comboBoxIdClient = new JComboBox<Integer>();
		comboBoxIdClient.setBounds(22, 30, 86, 20);
		contentPane.add(comboBoxIdClient);
		
		comboBoxIdProduct = new JComboBox<Integer>();
		comboBoxIdProduct.setBounds(22, 80, 86, 20);
		contentPane.add(comboBoxIdProduct);
		
		fill_idclient();
		fill_idproduct();
		
		/**
		 * - operatia insert order e deja definita in OrderDAO,de aceea butonul preia informatiile id_client,id_product din combobox-uri si quantity dintr-un JTextField si apeleaza aceasta operatie prin intermediul unui obiect de tip OrderBLL;
		 */
		JButton btnCreateOrder = new JButton("Create order");
		btnCreateOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					int id_client=(Integer)comboBoxIdClient.getSelectedItem();
					int id_product=(Integer)comboBoxIdProduct.getSelectedItem();
					int quantity=Integer.parseInt(textField_quantity.getText());
					OrderBLL o=new OrderBLL();
					o.insertOrder(new Order(id_client,id_product,quantity));
				}
				catch(Exception e1)
				{
					JOptionPane.showMessageDialog(null,e1.getMessage());
				}
			}
		});
		btnCreateOrder.setBounds(10, 170, 110, 23);
		contentPane.add(btnCreateOrder);
	
	}
}
