package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bll.ProductBLL;
import connection.ConnectionFactory;
import model.Product;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 05, 2017
 * 
 * -are 5 butoane corezpunzatoare unor operatii efectuate pe tabelul product din baza de date:add product,delete product,edit product,view all products(afisate intr-un Jtable),clear table;
 * -operatiile add product,delete product,edit product, clear table sunt deja definite in ProductDAO,de aceea butoanele preiau informatiile din JTextField-uri si apeleaza aceste operatii prin intermediul unui obiect de tip ProductBLL;
 */

public class ProductGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldAddN;
	private JTextField textFieldAddP;
	private JTextField textFieldAddS;
	private JTextField textFieldEditId;
	private JTextField textFieldDelId;
	private JTextField textFieldEditN;
	private JTextField textFieldEditP;
	private JTextField textFieldEditS;
	
	Connection dbConnection=null;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductGUI frame = new ProductGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ProductGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 684, 476);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBounds(0, 0, 673, 432);
		contentPane.add(panel);
		
		JLabel label = new JLabel("name:");
		label.setBounds(138, 28, 46, 14);
		panel.add(label);
		
		textFieldAddN = new JTextField();
		textFieldAddN.setColumns(10);
		textFieldAddN.setBounds(185, 25, 86, 20);
		panel.add(textFieldAddN);
		
		JLabel lblPrice = new JLabel("price:");
		lblPrice.setBounds(291, 28, 40, 14);
		panel.add(lblPrice);
		
		textFieldAddP = new JTextField();
		textFieldAddP.setColumns(10);
		textFieldAddP.setBounds(330, 25, 86, 20);
		panel.add(textFieldAddP);
		
		JLabel lblStock = new JLabel("stock:");
		lblStock.setBounds(426, 28, 46, 14);
		panel.add(lblStock);
		
		textFieldAddS = new JTextField();
		textFieldAddS.setColumns(10);
		textFieldAddS.setBounds(482, 25, 111, 20);
		panel.add(textFieldAddS);
		
		JLabel label_3 = new JLabel("id:");
		label_3.setBounds(120, 71, 24, 14);
		panel.add(label_3);
		
		textFieldEditId = new JTextField();
		textFieldEditId.setColumns(10);
		textFieldEditId.setBounds(138, 68, 40, 20);
		panel.add(textFieldEditId);
		
		textFieldDelId = new JTextField();
		textFieldDelId.setColumns(10);
		textFieldDelId.setBounds(123, 104, 40, 20);
		panel.add(textFieldDelId);
		
		JLabel label_4 = new JLabel("id:");
		label_4.setBounds(97, 107, 46, 14);
		panel.add(label_4);
		
		JLabel label_1 = new JLabel("name:");
		label_1.setBounds(197, 71, 46, 14);
		panel.add(label_1);
		
		textFieldEditN = new JTextField();
		textFieldEditN.setColumns(10);
		textFieldEditN.setBounds(244, 68, 86, 20);
		panel.add(textFieldEditN);
		
		JLabel label_2 = new JLabel("price:");
		label_2.setBounds(350, 71, 40, 14);
		panel.add(label_2);
		
		textFieldEditP = new JTextField();
		textFieldEditP.setColumns(10);
		textFieldEditP.setBounds(389, 68, 86, 20);
		panel.add(textFieldEditP);
		
		JLabel label_5 = new JLabel("stock:");
		label_5.setBounds(485, 71, 46, 14);
		panel.add(label_5);
		
		textFieldEditS = new JTextField();
		textFieldEditS.setColumns(10);
		textFieldEditS.setBounds(541, 68, 111, 20);
		panel.add(textFieldEditS);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 194, 642, 227);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnAddProduct = new JButton("Add product");
		btnAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					String name=textFieldAddN.getText();
					Float price=Float.parseFloat(textFieldAddP.getText());
					int stock=Integer.parseInt(textFieldAddS.getText());
					ProductBLL p=new ProductBLL();
					p.insertProduct(new Product(name,price,stock));
				}
				catch(Exception e1)
				{
					JOptionPane.showMessageDialog(null,e1.getMessage());
				}
			}
		});
		btnAddProduct.setBounds(0, 24, 128, 23);
		panel.add(btnAddProduct);
		
		JButton btnEditProduct = new JButton("Edit product");
		btnEditProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Float price=0.0f;
				int stock=-1;
				try
				{
					int id=Integer.parseInt(textFieldEditId.getText());
					String name=textFieldEditN.getText();
					if(textFieldEditP.getText().length()!=0)
					{
						price=Float.parseFloat(textFieldEditP.getText());
					}
					if(textFieldEditS.getText().length()!=0)
					{
						stock=Integer.parseInt(textFieldEditS.getText());
					}
					ProductBLL p=new ProductBLL();
					p.editProduct(new Product(id,name,price,stock));
				}
				catch(Exception e2)
				{
					JOptionPane.showMessageDialog(null,e2.getMessage());
				}
			}
		});
		btnEditProduct.setBounds(0, 67, 110, 23);
		panel.add(btnEditProduct);
		
		JButton buttonDel = new JButton("Delete");
		buttonDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					int id=Integer.parseInt(textFieldDelId.getText());
					ProductBLL p=new ProductBLL();
					p.deleteProduct(id);
				}
				catch(Exception e3)
				{
					JOptionPane.showMessageDialog(null,e3.getMessage());
				}
			}
		});
		buttonDel.setBounds(0, 104, 89, 23);
		panel.add(buttonDel);
		
		JButton btnViewAllProducts = new JButton("View all products");
		btnViewAllProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 dbConnection=ConnectionFactory.getConnection();
					
					PreparedStatement viewallstatement = null;
					try {
						viewallstatement = dbConnection.prepareStatement("SELECT * FROM product");
						ResultSet rs = viewallstatement.executeQuery();
						
						table.setModel(DbUtils.resultSetToTableModel(rs));
						
					} catch (Exception e6) {
						JOptionPane.showMessageDialog(null,e6.getMessage());
					} finally {
						ConnectionFactory.close(viewallstatement);
						ConnectionFactory.close(dbConnection);
					}
			}
		});
		btnViewAllProducts.setBounds(10, 150, 199, 23);
		panel.add(btnViewAllProducts);
		
		/**
		 * -butonul afiseaza toate produsele din baza de date intr-un Jtable folosind modelul DbUtils.resultSetToTableModel din libraria rs2xml.jar
		 */
		JButton button_4 = new JButton("Clear table");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					ProductBLL p=new ProductBLL();
					p.clearProduct();
				}
				catch(Exception e4)
				{
					JOptionPane.showMessageDialog(null,e4.getMessage());
				}
			}
		});
		button_4.setBounds(420, 150, 89, 23);
		panel.add(button_4);
	}
}
