package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bll.ClientBLL;
import connection.ConnectionFactory;
import model.Client;
import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 05, 2017
 * 
 * are 5 butoane corezpunzatoare unor operatii efectuate pe tabelul client din baza de date:add client,delete client,edit client,view all clients(afisati intr-un Jtable),clear table;
 * -operatiile add client,delete client,edit client, clear table sunt deja definite in ClientDAO,de aceea butoanele preiau informatiile din JTextField-uri si apeleaza aceste operatii prin intermediul unui obiect de tip ClientBLL;
 */

public class ClientGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldAddN;
	private JTextField textFieldAddA;
	private JTextField textFieldAddE;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientGUI frame = new ClientGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection dbConnection=null;
	private JTextField textFieldEditId;
	private JTextField textFieldDelId;
	private JTextField textFieldEditN;
	private JTextField textFieldEditA;
	private JTextField textFieldEditE;
	/**
	 * Create the frame.
	 */
	public ClientGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 689, 471);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("name:");
		lblName.setBounds(97, 28, 46, 14);
		contentPane.add(lblName);
		
		textFieldAddN = new JTextField();
		textFieldAddN.setBounds(135, 25, 86, 20);
		contentPane.add(textFieldAddN);
		textFieldAddN.setColumns(10);
		
		JLabel lblAddress = new JLabel("address:");
		lblAddress.setBounds(234, 29, 58, 14);
		contentPane.add(lblAddress);
		
		textFieldAddA = new JTextField();
		textFieldAddA.setColumns(10);
		textFieldAddA.setBounds(284, 26, 86, 20);
		contentPane.add(textFieldAddA);
		
		JLabel lblEmail = new JLabel("email:");
		lblEmail.setBounds(402, 28, 46, 14);
		contentPane.add(lblEmail);
		
		textFieldAddE = new JTextField();
		textFieldAddE.setBounds(441, 25, 222, 20);
		contentPane.add(textFieldAddE);
		textFieldAddE.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 196, 642, 236);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblId = new JLabel("id:");
		lblId.setBounds(97, 71, 46, 14);
		contentPane.add(lblId);
		
		textFieldEditId = new JTextField();
		textFieldEditId.setBounds(123, 68, 40, 20);
		contentPane.add(textFieldEditId);
		textFieldEditId.setColumns(10);
		
		textFieldDelId = new JTextField();
		textFieldDelId.setColumns(10);
		textFieldDelId.setBounds(123, 104, 40, 20);
		contentPane.add(textFieldDelId);
		
		JLabel label = new JLabel("id:");
		label.setBounds(97, 107, 46, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("name:");
		label_1.setBounds(173, 70, 35, 14);
		contentPane.add(label_1);
		
		textFieldEditN = new JTextField();
		textFieldEditN.setColumns(10);
		textFieldEditN.setBounds(216, 67, 86, 20);
		contentPane.add(textFieldEditN);
		
		JLabel label_2 = new JLabel("address:");
		label_2.setBounds(315, 71, 58, 14);
		contentPane.add(label_2);
		
		textFieldEditA = new JTextField();
		textFieldEditA.setColumns(10);
		textFieldEditA.setBounds(365, 68, 86, 20);
		contentPane.add(textFieldEditA);
		
		JLabel label_3 = new JLabel("email:");
		label_3.setBounds(463, 70, 66, 14);
		contentPane.add(label_3);
		
		textFieldEditE = new JTextField();
		textFieldEditE.setColumns(10);
		textFieldEditE.setBounds(498, 67, 165, 20);
		contentPane.add(textFieldEditE);
		
		JButton btnAddClient = new JButton("Add client");
		btnAddClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try
				{
					String name=textFieldAddN.getText();
					String address=textFieldAddA.getText();
					String email=textFieldAddE.getText();
					ClientBLL c=new ClientBLL();
					c.insertClient(new Client(name,address,email));
				}
				catch(Exception e)
				{
					JOptionPane.showMessageDialog(null,e.getMessage());
				}
			}
		});
		btnAddClient.setBounds(0, 24, 89, 23);
		contentPane.add(btnAddClient);
		
		JButton btnEditClient = new JButton("Edit client");
		btnEditClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=null;
				String address=null;
				String email=null;
				try
				{
					int id=Integer.parseInt(textFieldEditId.getText());
					name=textFieldEditN.getText();
					address=textFieldEditA.getText();
					email=textFieldEditE.getText();
					ClientBLL c=new ClientBLL();
					c.editClient(new Client(id,name,address,email));
				}
				catch(Exception e1)
				{
					JOptionPane.showMessageDialog(null,e1.getMessage());
				}
			}
		});
		btnEditClient.setBounds(0, 67, 89, 23);
		contentPane.add(btnEditClient);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					int id=Integer.parseInt(textFieldDelId.getText());
					ClientBLL c=new ClientBLL();
					c.deleteClient(id);
				}
				catch(Exception e2)
				{
					JOptionPane.showMessageDialog(null,e2.getMessage());
				}
			}
		});
		btnDelete.setBounds(0, 104, 89, 23);
		contentPane.add(btnDelete);
		
		JButton btnNewButton = new JButton("View all clients");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				    dbConnection=ConnectionFactory.getConnection();
				
					PreparedStatement viewallstatement = null;
					try {
						viewallstatement = dbConnection.prepareStatement("SELECT * FROM client");
						ResultSet rs = viewallstatement.executeQuery();
						
						table.setModel(DbUtils.resultSetToTableModel(rs));
						
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null,e.getMessage());
					} finally {
						ConnectionFactory.close(viewallstatement);
						ConnectionFactory.close(dbConnection);
					}
			}
		});
		btnNewButton.setBounds(10, 150, 199, 23);
		contentPane.add(btnNewButton);
		
		/**
		 * -butonul afiseaza toti clientii din baza de date intr-un Jtable folosind modelul DbUtils.resultSetToTableModel din libraria rs2xml.jar
		 */
		JButton btnClearTable = new JButton("Clear table");
		btnClearTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					ClientBLL c=new ClientBLL();
					c.clearClient();
				}
				catch(Exception e3)
				{
					JOptionPane.showMessageDialog(null,e3.getMessage());
				}
			}
		});
		btnClearTable.setBounds(420, 150, 89, 23);
		contentPane.add(btnClearTable);
	}
}
