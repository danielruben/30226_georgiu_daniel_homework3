package bll;

import java.util.NoSuchElementException;
import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -are un atribut privat de tip Validator<Client>:emailValidator;
 */

public class ClientBLL 
{
	private Validator<Client> emailValidator;
	
	/**
	 * -constructorul initializeaza atributul emailValidator;
	 */
	
	public ClientBLL()
	{
		emailValidator=new EmailValidator();
	}
	
	/**
	 * -metoda gaseste clientul pe baza id-ului folosind metoda findById din ClientDAO
	 * @param id
	 * @return foundClient
	 */
	public Client findClientById(int id) {
		Client client = ClientDAO.findById(id);
		if (client == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return client;
	}

	/**
	 * -metoda verifica daca emailul e corect si apoi insereaza clientul dat ca parametru in baza de date folosind metoda insert din ClientDAO
	 * @param c
	 * @return insertedId
	 */
	public int insertClient(Client c) {
		emailValidator.validate(c);
		return ClientDAO.insert(c);
	}
	
	/**
	 *  -metoda verifica daca emailul e corect si apoi editeaza clientul dat ca parametru folosind metoda editById din ClientDAO
	 * @param c
	 * @return ClientDAO.editById(c.getId(), c.getName(), c.getAddress(), c.getEmail())
	 */
	public int editClient(Client c) {
		if(c.getEmail().length()!=0)
		{
			emailValidator.validate(c);
		}
		return ClientDAO.editById(c.getId(), c.getName(), c.getAddress(), c.getEmail());
	}
	
	/**
	 * -metoda sterge clientul pe baza id-ului folosind metoda delete din ClientDAO
	 * @param id
	 * @return ClientDAO.delete(id)
	 */
	public int deleteClient(int id)
	{
		return ClientDAO.delete(id);
	}
	
	/**
	 * -metoda sterge toti clientii din baza de date folosind metoda clear din ClientDAO
	 * @return ClientDAO.clear()
	 */
	public int clearClient() 
	{
		return ClientDAO.clear();
	}
}
