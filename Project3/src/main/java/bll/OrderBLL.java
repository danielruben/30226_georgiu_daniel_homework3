package bll;


import java.util.NoSuchElementException;

import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import model.Order;

/**
 * 
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -are un atribut privat de tip Validator<Order>: quantityValidator;
 */

public class OrderBLL
{
	private Validator<Order> quantityValidator;
	
	/**
	 * -constructorul initializeaza atributul quantityValidator;
	 */
	public OrderBLL()
	{
		quantityValidator=new QuantityValidator();
	}
	
	/**
	 * -metoda gaseste comanda pe baza id-ului folosind metoda findById din OrderDAO
	 * @param id
	 * @return foundOrder
	 */
	public Order findOrderById(int id) {
		Order order = OrderDAO.findById(id);
		if (order == null) {
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		}
		return order;
	}

	/**
	 * -metoda verifica daca cantitatea e corecta si apoi insereaza comanda data ca parametru in baza de date folosind metoda insert din OrderDAO
	 * @param o
	 * @return OrderDAO.insert(o)
	 */
	public int insertOrder(Order o) 
	{
		quantityValidator.validate(o);
		return OrderDAO.insert(o);
	}
	
	/**
	 * -metoda sterge toate comenziile din baza de date folosind metoda clear din OrderDAO
	 * @return  OrderDAO.clear()
	 */
	public int clearOrder() 
	{
		return OrderDAO.clear();
	}
}
