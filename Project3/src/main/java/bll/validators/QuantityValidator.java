package bll.validators;

import model.Order;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * -implementeaza interfata Validator<Order>
 */

public class QuantityValidator implements Validator<Order>
{
	/**
	 * -verifica daca cantitatea introdusa de utilizator e corecta(>0)
	 */
	public void validate(Order ord) {

		if (ord.getQuantity()<=0) {
			throw new IllegalArgumentException("The Quantity must be higher than 0!");
		}

	}
}
