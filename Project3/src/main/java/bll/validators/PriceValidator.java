package bll.validators;

import model.Product;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -implementeaza interfata Validator<Product>
 */

public class PriceValidator implements Validator<Product>
{
	/**
	 * -verifica daca pretul introdus de utilizator e corect(>0)
	 */
	
	public void validate(Product p) {

		if (p.getPrice()<=0) {
			throw new IllegalArgumentException("The Product Price must be higher than 0!");
		}

	}
}