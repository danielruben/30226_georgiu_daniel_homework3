package bll.validators;
import model.Product;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -implementeaza interfata Validator<Product>
 */

public class StockValidator implements Validator<Product>
{
	/**
	 * -verifica daca stocul introdus de utilizator e corect(>=0)
	 */
	
	public void validate(Product p) {

		if (p.getStock()<0) {
			throw new IllegalArgumentException("The Product Stock can't be lower than 0!");
		}

	}

}
