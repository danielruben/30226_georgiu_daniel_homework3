package bll.validators;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -are o singura metoda de implementat:  public void validate(T t);
 */

public interface Validator<T> {

	public void validate(T t);
}
