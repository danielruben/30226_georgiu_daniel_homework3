package bll;

import java.util.NoSuchElementException;
import bll.validators.PriceValidator;
import bll.validators.StockValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -are 2 atribute private de tip Validator<Product>: priceValidator  si stockValidator;
 */

public class ProductBLL 
{
	private Validator<Product> priceValidator;
	private Validator<Product> stockValidator;
	
	/**
	 * -constructorul initializeaza atributele priceValidator  si stockValidator;
	 */
	public ProductBLL()
	{
		priceValidator=new PriceValidator();
		stockValidator=new StockValidator();
	}
	
	/**
	 * -metoda gaseste produsul pe baza id-ului folosind metoda findById din ProductDAO
	 * @param id
	 * @return foundProduct
	 */
	public Product findProductById(int id) {
		Product p = ProductDAO.findById(id);
		if (p == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return p;
	}

	/**
	 * -metoda verifica daca pretul si stocul sunt corecte si apoi insereaza produsul dat ca parametru in baza de date folosind metoda insert din ProductDAO
	 * @param p
	 * @return insertedId
	 */
	public int insertProduct(Product p) {
		priceValidator.validate(p);
		stockValidator.validate(p);
		return ProductDAO.insert(p);
	}
	
	/**
	 * -metoda editeaza produsul dat ca parametru folosind metoda editById din ProductDAO
	 * @param p
	 * @return ProductDAO.editById(p.getId(),p.getName(),p.getPrice(),p.getStock())
	 */
	public int editProduct(Product p) {
		return ProductDAO.editById(p.getId(),p.getName(),p.getPrice(),p.getStock());
	}
	
	/**
	 * -metoda sterge produsul pe baza id-ului folosind metoda delete din ProductDAO
	 * @param id
	 * @return ProductDAO.delete(id)
	 */
	public int deleteProduct(int id)
	{
		return ProductDAO.delete(id);
	}
	
	/**
	 * -metoda sterge toate produsele din baza de date folosind metoda clear din ProductDAO
	 * @return ProductDAO.clear()
	 */
	public int clearProduct() 
	{
		return ProductDAO.clear();
	}
}
