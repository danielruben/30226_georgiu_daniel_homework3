package connection;


/**
 * @Author: Georgiu Daniel
 * @Since: Mai 04, 2017
 * 
 * -realizeaza conectarea la baza de date
 * Atribute:
 * -un atribut private static final de tip logger: LOGGER
 * -4 atribute private static final de tip String:DRIVER,DBURL,USER,PASS
 * -un atribut private static de tip ConnectionFactory:singleInstance
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class ConnectionFactory {

	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse?useSSL=false";
	private static final String USER = "root";
	private static final String PASS = "roottp";

	private static ConnectionFactory singleInstance = new ConnectionFactory();

	private ConnectionFactory() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * 	
	 * -metoda realizeaza conexiunea la baza de date;
	 * @return connection;
	 */
	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(DBURL, USER, PASS);
			//JOptionPane.showMessageDialog(null,"connect ok");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			//JOptionPane.showMessageDialog(null,"An error occured while trying to connect to the database");
			e.printStackTrace();
		}
		return connection;
	}

	/**
	 * 
	 * @return conexiunea la baza de date;
	 */
	
	public static Connection getConnection() {
		return singleInstance.createConnection();
	}

	/**
	 * -metoda inchide conexiunea la baza de date;
	 * @param connection
	 */
	
	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
			}
		}
	}

	/**
	 * -metoda inchide o interogare efectuata asupra bazei de date;
	 * @param statement
	 */
	
	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
			}
		}
	}

	/**
	 * -metoda inchide rezultatul obtinut de catre o interogare efectuata asupra bazei de date;
	 * @param resultSet
	 */
	
	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
			}
		}
	}
}