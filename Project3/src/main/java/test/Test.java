package test;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Product;
import model.Order;

public class Test {

	public static void main(String[] args)
	{
		ClientBLL clientMan=new ClientBLL();
		clientMan.clearClient();
		Client c1=new Client("Client1","Addres1","client1@google.com");
		Client c2=new Client("Client2","Addres2","client2@google.com");
		Client c3=new Client("Client3","Addres3","client3@google.com");
		clientMan.insertClient(c1);
		clientMan.insertClient(c2);
		clientMan.insertClient(c3);
		System.out.println(clientMan.findClientById(2).toString());
		//clientMan.deleteClient(2);
		//System.out.println(clientMan.findClientById(2).toString());
		//clientMan.editClient(new Client(1,"Client7",null,"client11@google.com"));
		System.out.println(clientMan.findClientById(1).toString());
		
		ProductBLL productMan=new ProductBLL();
		productMan.clearProduct();
		Product p1=new Product("Product1",2.5f,100);
		Product p2=new Product("Product2",5.7f,15);
		Product p3=new Product("Product3",7.2f,58);
		productMan.insertProduct(p1);
		productMan.insertProduct(p2);
		productMan.insertProduct(p3);
		System.out.println(productMan.findProductById(1).toString());
		//productMan.deleteProduct(1);
		//System.out.println(productMan.findProductById(1).toString());
		//productMan.editProduct(new Product(2,"Product12",0,18));
		System.out.println(productMan.findProductById(2).toString());
		
		OrderBLL orderMan=new OrderBLL();
		orderMan.clearOrder();
		/*Order o1=new Order(2,1,3);
		orderMan.insertOrder(o1);
		System.out.println(orderMan.findOrderById(1).toString());
		Order o2=new Order(3,2,2);
		orderMan.insertOrder(o2);
		System.out.println(orderMan.findOrderById(2).toString());*/
	}

}
